# Static site for PyCon Israel 2019

### Contribute
1. Install [lektor](https://www.getlektor.com/docs/installation/) on your desktop.
1. Fork and clone this repo.
1. Start lektor `lektor --project <forked_repo_path> server`.
1. Add content (the website should be available at http://127.0.0.1:5000).
1. Build the site `lektor --project <forked_repo_path> build`.
1. Commit, push and create MR.