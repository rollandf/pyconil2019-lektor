title: Code of Conduct
---
body:

<div class="page">
<h1 class="d-flex justify-content-between">
    Conference Code of Conduct

</h1>
<div class="page-content">
    <p>All attendees, speakers, sponsors and volunteers at our conference are kindly asked to adopt the following code of conduct, and are required to agree to the Anti-Harassment Policy. We are expecting cooperation from all participants to help ensuring a safe and comfortable environment for everybody.</p>
<h2>The Quick Version</h2>
<p>Be friendly, patient, welcoming and considerate; be respectful, use appropriate language, and avoid discriminatory behavior or expressions. In particular, harassment is unacceptable; our conference is dedicated to providing a harassment-free conference experience for everyone, and participants violating the Anti-Harassment Policy may be sanctioned or even expelled from the conference without a refund, at the discretion of the conference organisers.</p>
<h2>The Full Version</h2>
<ul>
<li>Be friendly and welcoming. We strive to be a community that welcomes and supports people of all backgrounds and identities.</li>
<li>Be patient and considerate. We all want to have fun teaching and learning, and a cooperative atmosphere will make us more productive.</li>
<li>Be respectful. Not all of us will agree all the time, but disagreement is no excuse for poor behavior and poor manners. Refrain from personal attacks, and be respectful when dealing with other members of the Python community as well as with people outside it.</li>
<li>Use appropriate language, and avoid discriminatory behavior. Everybody is welcome to this conference, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, or religion (or lack thereof).</li>
<li>We do not tolerate harassment of conference participants in any form. Organisers will enforce the Anti-Harassment Policy throughout the event.</li>
</ul>
<h2>Anti-Harassment Policy</h2>
<ul>
<li>Harassment includes offensive comments related to gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, or religion (or lack thereof); deliberate intimidation, stalking, following, harassing photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention. Sexual language and imagery is not appropriate for any conference venue, including talks, workshops, parties and online media.</li>
<li>Participants asked to stop any harassing behavior are expected to comply immediately.</li>
<li>Sponsors are also subject to the Anti-Harassment Policy. In particular, sponsors should not use sexualised images, activities, or other material. Booth staff (including volunteers) should not use sexualised clothing/uniforms/costumes, or otherwise create a sexualised environment.</li>
<li>If a participant engages in harassing behavior, the conference organisers may take any action they deem appropriate, including warning the offender or even expulsion from the conference with no refund.</li>
</ul>
<h2>Need Help?</h2>
<p>If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact a member of conference staff immediately. Conference staff can be identified by the printing on the back of their shirts.</p>
<p>If the matter is especially urgent, please call the conference staff (contact phones will be added here before the conference):</p>
<ul>
<li>Anat Levi</li>
<li>Moshe Nahmias</li>
</ul>
<p>Conference staff will be happy to help participants contact hotel/venue security or local law enforcement, provide escorts, or otherwise assist those experiencing harassment to feel safe for the duration of the conference. We value your attendance.</p>
<p>We expect participants to follow these rules at conference and workshop venues and conference-related social events.</p>
</div>
</div>
</div>
---
author: PyCon Israel Organizing team
---
pub_date: 2019-04-02
